export interface PersonaInterface {
  id?: string;
  nombre?: string;
  apellido?: string;
  fechaNacimiento?: Date;
  estadoCivil?: 'soltero' | 'casado' | 'viudo' | 'divorciado';
  mascota?: {nombre: string}[]
}

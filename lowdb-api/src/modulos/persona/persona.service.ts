import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { PersonaInterface } from './interface/persona.interface';
import { obtenerConexion } from '../../conexion-lowdb/conexion-lowdb';

@Injectable()
export class PersonaService {

  async crearPersona(persona: PersonaInterface): Promise<PersonaInterface> {
    try {
      const respuesta: PersonaInterface[] = await obtenerConexion()
        .get('persona')
        .push(persona)
        .write();

      const indiceNuevo = respuesta.indexOf(persona);
      return respuesta[indiceNuevo];
    } catch (e) {
      throw new InternalServerErrorException({
        error: e,
        mensaje: 'No se pudo guardar a persona',
      });
    }
  };

  async actualizarPersona(personaActualizada: PersonaInterface, idPersona: string): Promise<PersonaInterface | string> {
    const consulta = {
      id: idPersona,
    };
    try {
      const respuestaBusqueda = await obtenerConexion()
        .get('persona')
        .find(consulta)
        .value();

      const existePersona = respuestaBusqueda !== undefined;

      if (existePersona) {
        const respuestaActualizado = await obtenerConexion()
          .get('persona')
          .find(consulta)
          .assign(personaActualizada)
          .write();
        return respuestaActualizado;
      } else {
        return new Promise((resolve, reject) => {
          resolve('No se ha encontrado persona a actualizar');
        });
      }
    } catch (e) {
      throw new InternalServerErrorException({
        error: e,
        mensaje: 'No se pudo actualizar persona',
      });
    }
  };

  async obtenerTodasPersonas(): Promise<PersonaInterface[] | string> {
    try {
      const arregloPersonas: PersonaInterface[] = await obtenerConexion()
        .get('persona')
        .value();
      const existePersonas = arregloPersonas.length > 0;
      if (existePersonas) {
        return arregloPersonas;
      } else {
        return new Promise((resolve, reject) => {
          resolve('No existen personas');
        });
      }
    } catch (e) {
      throw new InternalServerErrorException({
        error: e,
        mensaje: 'Error al oobtner personas',
      });
    }
  }

  async obtenerPersonaPorId(idPersona: string): Promise<PersonaInterface | string> {
    const consulta = {
      id: idPersona,
    };
    try {
      const persona: PersonaInterface = await obtenerConexion()
        .get('persona')
        .find(consulta)
        .value();
      const existePersona = persona !== undefined;
      if (existePersona) {
        return persona;
      } else {
        return new Promise((resolve, reject) => {
          resolve('No existe persona');
        });
      }

    } catch (e) {
      throw new InternalServerErrorException({
        error: e,
        mensaje: 'Error al buscar persona por id',
      });
    }
  };

  async eliminarPersona(idPersona: string): Promise<any> {

    const consulta = {
      id: idPersona,
    };

    try {
      const respuestaBusqueda = await obtenerConexion()
        .get('persona')
        .find(consulta)
        .value();

      const existePersona = respuestaBusqueda !== undefined;

      if (existePersona) {
        const respuestaEliminar = await obtenerConexion()
          .get('persona')
          .remove(consulta)
          .write();
        return respuestaEliminar;
      } else {
        return new Promise((resolve, reject) => {
          resolve('No se encontro persona a eliminar');
        });
      }
    } catch (e) {
      throw new InternalServerErrorException({
        error: e,
        mensaje: 'No se pudo eliminar persona',
      });
    }
  };

}

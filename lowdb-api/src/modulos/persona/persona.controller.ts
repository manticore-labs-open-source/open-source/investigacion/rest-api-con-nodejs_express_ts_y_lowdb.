import {BadRequestException, Body, Controller, Delete, Get, Param, Post, Put} from '@nestjs/common';
import {PersonaService} from './persona.service';
import {PersonaInterface} from './interface/persona.interface';
import {v4} from 'uuid';

@Controller('persona')
export class PersonaController {

    constructor(
        private readonly _personaService: PersonaService,
    ) {
    }

    @Post()
    async crearPersona(
        @Body() datosPersona: PersonaInterface,
    ) {
        const existeDatos = datosPersona !== undefined
        if (existeDatos) {
            datosPersona['id'] = v4();
            const respuesta = await this._personaService.crearPersona(datosPersona);
            return respuesta;
        } else {
            throw new BadRequestException({
                mensaje: 'No envia datos de persona'
            })
        }
    }

    @Put(':idPersona')
    async actualizarPersona(
        @Param('idPersona') idPersona: string,
        @Body() datosPersona: PersonaInterface,
    ) {
        const existeDatos = datosPersona !== undefined && idPersona !== undefined;
        if (existeDatos) {
            const respuesta = await this._personaService.actualizarPersona(datosPersona, idPersona);
            return respuesta;
        } else {
            throw new BadRequestException({
                mensaje: 'No envia datos parametros'
            })
        }

    }

    @Get()
    async obtnerTodasPersonas() {
        const respuesta = this._personaService.obtenerTodasPersonas();
        return respuesta;
    }

    @Get(':idPersona')
    async obtenerPersonaPorId(
        @Param('idPersona') idPersona: string,
    ) {
        const existePersona = idPersona !== undefined;
        if (existePersona) {
            const respuesta = await this._personaService.obtenerPersonaPorId(idPersona);
            return respuesta;
        } else {
            throw new BadRequestException({
                mensaje: 'No envia id de persona',
            });
        }
    }

    @Delete(':idPersona')
    async eliminarPersona(
        @Param('idPersona') idPersona: string,
    ) {
        const existePersona = idPersona !== undefined;
        if (existePersona) {
            const respuesta = await this._personaService.eliminarPersona(idPersona);
            return respuesta;
        } else {
            throw new BadRequestException({
                mensaje: 'No envia id de persona',
            });
        }
    }


}

import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { crearConexionLowDb } from './conexion-lowdb/conexion-lowdb';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  crearConexionLowDb();
  await app.listen(3000);
}
bootstrap();

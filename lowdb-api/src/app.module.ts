import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PersonaModule } from './modulos/persona/persona.module';
@Module({
  imports: [PersonaModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}

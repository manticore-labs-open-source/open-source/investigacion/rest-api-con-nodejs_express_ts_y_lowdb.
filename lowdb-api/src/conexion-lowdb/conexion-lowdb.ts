import * as lowdb from 'lowdb';
import * as fileAsync from 'lowdb/adapters/FileAsync';

let baseDatos: lowdb.LowdbSync;

export async function crearConexionLowDb() {

    const adaptador = new fileAsync('db.json');
    baseDatos = await lowdb(adaptador);
    baseDatos.defaults({
        persona: [],
    })
        .write();
}

export const obtenerConexion = () => {
    return baseDatos;
};


## LowDB: Base de datos JSON para NodeJs

![](https://avatars1.githubusercontent.com/u/5502029?s=400&u=775f8ac3302e61c2c1fcb75894cc9a2c27c8f24a&v=4)

### Modulos y recursos utilizados.

* **NodeJs**: v12.17.0
* **LowDB**: v1.0.0
* **uuid**: v8.1.0
* **NestJs**: v7.2.0
* **SO**: Ubuntu 18.4
* **IDE**: Webstorm
* **Cliente**: Postman

### Repositorio

Para obtener el codigo fuente da clic [aqui](https://gitlab.com/manticore-labs-open-source/open-source/investigacion/rest-api-con-nodejs_express_ts_y_lowdb..git)


### Que es LowDB


**LowDB** es una librería que permite simular el comportamiento de una base de datos No SQL orientada a documentos,  usando para su acceso, un entorno NodeJS.

Para ello, la librería provee dos formas de hacerlo, usando un archivo JSON o usando la memoria RAM disponible. 


### Utilizar la libreria LowDB

1. Creamos un nuevo proyecto en NestJs:
 ```
 $ nest new lowdb-api
 ```

2. Cuando se haya creado el proyecto, podremos visualizar la siguiente estructura en nuestro proyecto:
 
 ![](https://gitlab.com/manticore-labs-open-source/open-source/investigacion/rest-api-con-nodejs_express_ts_y_lowdb./-/raw/master/imagenes/estructura.png)
 
4. Dentro del proyecto instalamos las dependencias:

```
 $ npm i lowdb uuid
```
* Uuid nos facilitara la generación de una *id* en los registros que creemos.

3. Ahora procedemos a crear el modulo **Persona**, con su respectivo `Servicio` y `Controlador`:
 
```
 $ nest generate module persona
 $ nest generate service persona --no-spec
 $ nest generate controller persona --no-spec
```
 * Agregamos *--no-spec* para deshabilitar la generación de archivos *spec*.

3. Importamos el modulo `Persona` dentro del  `app.module`.


4. Dentro del  directorio `/src` crearemos un archivo `*.ts` desde el cual generaremos la conexión con LowDb.
  
```
import * as lowdb from 'lowdb';
import * as fileAsync from 'lowdb/adapters/FileAsync';

let baseDatos: lowdb.LowdbSync;

export async function crearConexionLowDb() {
    const adaptador = new fileAsync('db.json');
    baseDatos = await lowdb(adaptador);
    baseDatos.defaults({
        persona: [],
    })
        .write();
}

export const obtenerConexion = () => {
    return baseDatos;
};
```
* **fileAsync** guardara la conexión Asíncrona, donde se creara el archivo para almacenar los datos.
* La función **crearConexion()** creara nuestra base de datos `db.json`.
* En **baseDatos.defaults** vamos a definir las tablas o esquemas que vamos a tener en nuestro json, en nuestro caso vamos a tener una llamada `persona` que va a ser un arreglo.


5. Ahora importamos la función `crearConexionLowDb()` dentro del archivo `main.ts`.

6. Procedemos a levantar nuestro proyecto:

```
 $ npm run start:dev
```

7. Comprobamos que se nos haya creado el archivo `db.json`, con la siguiente estructura:

![](https://gitlab.com/manticore-labs-open-source/open-source/investigacion/rest-api-con-nodejs_express_ts_y_lowdb./-/raw/master/imagenes/archivo-db.png)
```
{
  "persona": []
}
```

8. Para facilitar el manejo de los registros, creamos la interface **PersonaInterface**:

```
export interface PersonaInterface {
  id?: string;
  nombre?: string;
  apellido?: string;
  fechaNacimiento?: Date;
  estadoCivil?: 'soltero' | 'casado' | 'viudo' | 'divorciado';
  mascota?: {nombre: string}[]
}
```

9. Dentro del archivo `persona.service` creamos los métodos de crear, actualizar, buscar y eliminar:

  - Crear:
```
async crearPersona(persona: PersonaInterface): Promise<PersonaInterface> {
    try {
      const respuesta: PersonaInterface[] = await obtenerConexion()
        .get('persona')
        .push(persona)
        .write();

      const indiceNuevo = respuesta.indexOf(persona);
      return respuesta[indiceNuevo];
    } catch (e) {
      throw new InternalServerErrorException({
        error: e,
        mensaje: 'No se pudo guardar a persona',
      });
    }
  };
```
   - Actualizar:
```
  async actualizarPersona(personaActualizada: PersonaInterface, idPersona: string): Promise<PersonaInterface | string> {
    const consulta = {
      id: idPersona,
    };
    try {
      const respuestaBusqueda = await obtenerConexion()
        .get('persona')
        .find(consulta)
        .value();

      const existePersona = respuestaBusqueda !== undefined;

      if (existePersona) {
        const respuestaActualizado = await obtenerConexion()
          .get('persona')
          .find(consulta)
          .assign(personaActualizada)
          .write();
        return respuestaActualizado;
      } else {
        return new Promise((resolve, reject) => {
          resolve('No se ha encontrado persona a actualizar');
        });
      }
    } catch (e) {
      throw new InternalServerErrorException({
        error: e,
        mensaje: 'No se pudo actualizar persona',
      });
    }
  };
```

   - Buscar todos los registros:

```
  async obtenerTodasPersonas(): Promise<PersonaInterface[] | string> {
    try {
      const arregloPersonas: PersonaInterface[] = await obtenerConexion()
        .get('persona')
        .value();
      const existePersonas = arregloPersonas.length > 0;
      if (existePersonas) {
        return arregloPersonas;
      } else {
        return new Promise((resolve, reject) => {
          resolve('No existen personas');
        });
      }
    } catch (e) {
      throw new InternalServerErrorException({
        error: e,
        mensaje: 'Error al oobtner personas',
      });
    }
  }

```
 
   - Buscar por id:

```
  async obtenerPersonaPorId(idPersona: string): Promise<PersonaInterface | string> {
    const consulta = {
      id: idPersona,
    };
    try {
      const persona: PersonaInterface = await obtenerConexion()
        .get('persona')
        .find(consulta)
        .value();
      const existePersona = persona !== undefined;
      if (existePersona) {
        return persona;
      } else {
        return new Promise((resolve, reject) => {
          resolve('No existe persona');
        });
      }

    } catch (e) {
      throw new InternalServerErrorException({
        error: e,
        mensaje: 'Error al buscar persona por id',
      });
    }
  };

```

   - Eliminar:

```
  async eliminarPersona(idPersona: string): Promise<any> {

    const consulta = {
      id: idPersona,
    };

    try {
      const respuestaBusqueda = await obtenerConexion()
        .get('persona')
        .find(consulta)
        .value();

      const existePersona = respuestaBusqueda !== undefined;

      if (existePersona) {
        const respuestaEliminar = await obtenerConexion()
          .get('persona')
          .remove(consulta)
          .write();
        return respuestaEliminar;
      } else {
        return new Promise((resolve, reject) => {
          resolve('No se encontro persona a eliminar');
        });
      }
    } catch (e) {
      throw new InternalServerErrorException({
        error: e,
        mensaje: 'No se pudo eliminar persona',
      });
    }
  };
```

10. Y ahora en el controlador crearemos las rutas básicas a traves de las cuales se recibirá la solicitud del cliente:

| Método(ruta) | Descripción |
| ------ | ------ |
| get('/persona' | Obtener todas las personas. |
| get('/persona/:idPersona') | Buscar persona por id. |
| post('/persona') | Crear una persona. |
| put('/persona/:idPersona') | Actualizar una persona. |
| delete('/persona/:idPersona') | Eliminar una persona por id. |


   - Importamos nuestro servicio, en el *constructor* del archivo `persona.controller`.

```
@Controller('persona')
export class PersonaController {

  constructor(
    private readonly _personaService: PersonaService,
  ) {
  }
}
```

   - Creamos las rutas:
   
   - Crear:
      - Para generar la `id` unica de manera automatica utilizamos `v4()` del modulo **uuid**.
   ```
    @Post()
    async crearPersona(
        @Body() datosPersona: PersonaInterface,
    ) {
        const existeDatos = datosPersona !== undefined
        if (existeDatos) {
            datosPersona['id'] = v4();
            const respuesta = await this._personaService.crearPersona(datosPersona);
            return respuesta;
        } else {
            throw new BadRequestException({
                mensaje: 'No envia datos de persona'
            })
        }
    }
   ```
   
   - Actualizar:
   ```
    @Put(':idPersona')
    async actualizarPersona(
        @Param('idPersona') idPersona: string,
        @Body() datosPersona: PersonaInterface,
    ) {
        const existeDatos = datosPersona !== undefined && idPersona !== undefined;
        if (existeDatos) {
            const respuesta = await this._personaService.actualizarPersona(datosPersona, idPersona);
            return respuesta;
        } else {
            throw new BadRequestException({
                mensaje: 'No envia datos parametros'
            })
        }

    }

   ```

   - Obtener todos:
   ```
    @Get()
    async obtnerTodasPersonas() {
        const respuesta = this._personaService.obtenerTodasPersonas();
        return respuesta;
    }
   ```
   
   - Obtener por id:
   ```
    @Get(':idPersona')
    async obtenerPersonaPorId(
        @Param('idPersona') idPersona: string,
    ) {
        const existePersona = idPersona !== undefined;
        if (existePersona) {
            const respuesta = await this._personaService.obtenerPersonaPorId(idPersona);
            return respuesta;
        } else {
            throw new BadRequestException({
                mensaje: 'No envia id de persona',
            });
        }
    }
   ```
   
   - Eliminar:
   ```
    @Delete(':idPersona')
    async eliminarPersona(
        @Param('idPersona') idPersona: string,
    ) {
        const existePersona = idPersona !== undefined;
        if (existePersona) {
            const respuesta = await this._personaService.eliminarPersona(idPersona);
            return respuesta;
        } else {
            throw new BadRequestException({
                mensaje: 'No envia id de persona',
            });
        }
    }
   ```

11. Ahora comprobamos el funcionamiento, para lo cual simularemos al cliente a traves del programa **Postman**.

- Creamos un registro:

![](https://gitlab.com/manticore-labs-open-source/open-source/investigacion/rest-api-con-nodejs_express_ts_y_lowdb./-/raw/master/imagenes/crear.png)

- Listamos todos:

![](https://gitlab.com/manticore-labs-open-source/open-source/investigacion/rest-api-con-nodejs_express_ts_y_lowdb./-/raw/master/imagenes/listar-todo.png)

- Listamos por id:

![](https://gitlab.com/manticore-labs-open-source/open-source/investigacion/rest-api-con-nodejs_express_ts_y_lowdb./-/raw/master/imagenes/listar-por-id.png)

- Actualizamos:

![](https://gitlab.com/manticore-labs-open-source/open-source/investigacion/rest-api-con-nodejs_express_ts_y_lowdb./-/raw/master/imagenes/actualizar.png)


- Eliminamos:

![](https://gitlab.com/manticore-labs-open-source/open-source/investigacion/rest-api-con-nodejs_express_ts_y_lowdb./-/raw/master/imagenes/eliminar.png)



Realizado por:

<a href="https://www.linkedin.com/in/edwin-guamushig-6a9b08199/" target="_blank"><img alt="Sígueme en LinkedIn" height="35" width="35" src="https://4.bp.blogspot.com/-0KtSvK3BydE/XCrIzgI3RqI/AAAAAAAAH_w/n_rr5DS92uk9EWEegcxeqAcSkV36OWEOgCLcBGAs/s1600/linkedin.png" title="Sígueme en LinkedIn"/> Edwin Guamushig</a><br>
<a href="https://www.facebook.com/edwin7319" target="_blank"><img alt="Sígueme en Twitter" height="35" width="35" src="https://sguru.org/wp-content/uploads/2018/02/Facebook-PNG-Image-71244.png" title="Sígueme en facebook"/> Edwin Guamushig </a><br>
<a href="https://twitter.com/EdwinG7319" target="_blank"><img alt="Sígueme en Twitter" height="35" width="35" src="https://3.bp.blogspot.com/-E4jytrbmLbY/XCrI2Xd_hUI/AAAAAAAAIAo/qXt-bJg1UpMZmTjCJymxWEOGXWEQ2mv3ACLcBGAs/s1600/twitter.png" title="Sígueme en Twitter"/> @EdwinG7319 </a><br>
